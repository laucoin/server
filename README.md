# Server configuration

<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![All Contributors](https://img.shields.io/badge/all_contributors-1-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->

## This repository

This repository is a helpful tool, to make a basic serverside installation and configuration with Ansible.

## How to install and use it ?

### Prerequisites

1. Have a user access to the remote server (ssh)
2. The user should be able to run `sudo su`
3. Install ansible on your local environment
    - Linux (Debian/Ubuntu)
        ```bash
        sudo apt install ansible
        ```
    - Linux (CentOS)
        ```bash
        sudo yum install ansible
        ```
    - MacOS
        if needed, install [Homebrew](https://brew.sh/)
        ```bash
        brew install ansible
        ```

### Setup

1. Clone this repository with the following command:
    ```bash
    git clone git@gitlab.com:laucoin/server.git $GIT_PATH/Perso/server
    ```
    OR
    ```bash
    git clone https://gitlab.com/laucoin/server.git $GIT_PATH/Perso/server
    ```
2. Insert the target server(s) address into the [configuration file](inventory.ini)
3. Feel free to edit the config file, for example:
    - [motd](roles/ssh/files/motd)
    - [ssh banner](roles/ssh/files/banner.net)
    - [traefik config](roles/traefik/files/traefik.yml) (change the email `certificatesResolvers.letsencrypt.acme.email`)
4. Run the following command
    ```bash
    ansible-playbook --ask-become-pass -i $GIT_PATH/Perso/server/inventory.ini $GIT_PATH/Perso/server/ansible-playbook.yml --extra-vars "ssh_username=<your_user> override_root=<boolean>"
    ```

## Contributing

The `main` branch contains the stable code.

If you have more question, please have a look on [contributing file](https://gitlab.com/laucoin/global-readme/-/blob/main/CONTRIBUTING.md)

## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tbody>
    <tr>
      <td align="center"><a href="https://luc-aucoin.fr"><img src="https://avatars.githubusercontent.com/u/31480129?v=4?s=100" width="100px;" alt="Luc AUCOIN"/><br /><sub><b>Luc AUCOIN</b></sub></a><br /><a href="https://github.com/laucoin/git-resources/commits?author=laucoin" title="Code">💻</a> <a href="https://github.com/laucoin/git-resources/commits?author=laucoin" title="Documentation">📖</a> <a href="#maintenance-laucoin" title="Maintenance">🚧</a></td>
    </tr>
  </tbody>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification.
Contributions of any kind welcome!
